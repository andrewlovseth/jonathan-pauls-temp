<?php

/*
	Template Name: Splash
*/

get_header(); ?>


	<div class="background-photo">
		<img src="<?php $image = get_field('background_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<div class="info">
		<div class="grid">

			<div class="logo">
				<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="tagline">
				<p><?php the_field('tagline'); ?></p>
			</div>

			<div class="contact-info">
				<div class="detail hours">
					<h5>Hours</h5>
					<p><?php the_field('hours'); ?></p>
				</div>

				<div class="detail address">
					<h5>Address</h5>
					<p><?php the_field('address'); ?></p>
					<p class="map"><a href="<?php the_field('map_link'); ?>" target="_blank">Map</a></p>
				</div>

				<div class="detail phone">
					<h5>Phone</h5>
					<p><a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a></p>
				</div>

				<div class="detail email">
					<h5>Email</h5>
					<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
				</div>

			</div>

			<div class="site-message">
				<p><?php the_field('site_message'); ?></p>
			</div>


		</div>

	</div>


	
<?php get_footer(); ?>